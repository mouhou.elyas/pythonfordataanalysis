DROP TABLE IF EXISTS `myTable`;

CREATE TABLE `myTable` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `phone` varchar(100) default NULL,
  `email` varchar(255) default NULL,
  `country` varchar(100) default NULL,
  `numberrange` mediumint default NULL,
  `alphanumeric` varchar(255),
  `currency` varchar(100) default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("Lamar O'donnell","1-204-671-3888","diam@outlook.org","Pakistan",3,"MKS45ZSJ1JI","$35.41"),
  ("Dexter Pace","(922) 463-9097","ante.bibendum@yahoo.edu","Belgium",6,"NSJ15OPG5VV","$0.02"),
  ("Oleg Randolph","(764) 783-5721","nisl@aol.com","Netherlands",7,"IJH10ONO8RJ","$95.54"),
  ("David Garrison","1-144-418-1448","bibendum.sed@yahoo.net","Belgium",5,"XDR50LSB1TX","$18.43"),
  ("Nyssa Mcmahon","1-867-437-2872","quis.arcu.vel@protonmail.org","New Zealand",8,"BRB48CAQ6XU","$25.90"),
  ("Leila Patterson","(802) 324-4875","faucibus.lectus.a@google.edu","Ireland",0,"RYL72LWK1DX","$78.09"),
  ("Joy William","1-293-359-2813","eleifend.nec@google.com","India",2,"HRJ35ZGR0TE","$59.96"),
  ("Oscar Underwood","1-226-525-0517","imperdiet.ornare@yahoo.com","Colombia",5,"MEF28PHV0IY","$65.24"),
  ("Geoffrey Turner","1-216-316-6561","iaculis.quis@icloud.ca","Austria",9,"OIU53BNI4RR","$32.09"),
  ("Tashya Sears","(427) 540-2645","egestas@outlook.net","Vietnam",8,"VOB62CFV9OM","$29.84");
INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("Whitney Mckay","1-683-537-5372","nullam.vitae.diam@google.edu","Poland",1,"SUO89TLX4MQ","$49.34"),
  ("Nicole Mcmillan","(644) 262-8365","semper@protonmail.couk","New Zealand",7,"QKR88CFE7IG","$73.68"),
  ("Karina Mckee","1-520-166-2245","nec.tempus.scelerisque@outlook.net","Brazil",4,"XNN97XLS3DC","$98.75"),
  ("Dolan Weaver","(486) 726-6232","nulla.eu@outlook.couk","Sweden",3,"UEF88VVB5AB","$70.76"),
  ("Amelia Hunter","(887) 594-4895","lacinia@yahoo.net","Russian Federation",7,"VQR22WHQ2IV","$40.14"),
  ("Ulric Faulkner","(351) 389-5066","urna.convallis@protonmail.edu","Chile",2,"HIS53UFP1OX","$0.27"),
  ("Holmes Drake","1-313-243-2223","nunc.ac.sem@google.net","Belgium",4,"GKZ55FJG5WN","$92.00"),
  ("Morgan Benson","1-687-340-3365","aliquet.metus.urna@protonmail.org","Colombia",1,"GEH46TBA8WY","$80.43"),
  ("Tyler Berry","(586) 463-7832","phasellus.ornare.fusce@protonmail.org","Norway",4,"KQE92FYS4JQ","$91.42"),
  ("Aline Nielsen","1-244-334-4456","sed@outlook.com","India",6,"UFC58IFL7WE","$19.10");
INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("Colt Day","1-331-650-6844","semper@google.edu","Netherlands",7,"EFE83JGP9PH","$25.59"),
  ("Ulric Johnson","1-283-388-7817","dictum.proin@aol.couk","India",2,"HOD22NWY1MG","$31.25"),
  ("Brooke Delacruz","1-351-255-6761","sapien.aenean@outlook.couk","United States",7,"MKI17ENU1AE","$54.47"),
  ("Yvette Shaffer","1-665-471-7329","at.nisi.cum@icloud.edu","United States",9,"JPP32MTZ4GH","$77.57"),
  ("Stewart Stewart","(715) 548-8878","est.congue@hotmail.couk","Spain",1,"ZZL33LAU6WY","$89.73"),
  ("Keefe Sexton","1-262-221-5676","sagittis@icloud.edu","United States",8,"QJQ31WJB3ID","$36.35"),
  ("Alan Hall","(717) 456-3014","sed@outlook.couk","Turkey",7,"PVK86WXY7CU","$89.61"),
  ("Jerome Delaney","(327) 913-1719","sociis.natoque.penatibus@yahoo.edu","Norway",9,"CCI15WKY9KO","$88.81"),
  ("Mollie Clay","1-701-479-6429","gravida@hotmail.org","Belgium",8,"QXQ25IXR5JH","$89.21"),
  ("Quyn Mccoy","1-276-411-6120","imperdiet.ullamcorper@yahoo.com","Spain",7,"HRZ11CBR0KO","$83.78");
INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("Deacon Noble","(556) 686-7655","aliquam@google.org","Pakistan",10,"BOP32XHM7WX","$56.32"),
  ("Griffith Ramos","1-133-801-6021","ridiculus.mus@hotmail.org","Austria",1,"MMT76XOF6EM","$99.98"),
  ("Theodore Hendrix","1-948-546-9676","tristique@hotmail.couk","Australia",7,"UMS65NYE6BW","$80.47"),
  ("Sandra Ward","1-768-853-5675","tellus.nunc@aol.edu","Canada",5,"ZKV71CMM6PX","$53.97"),
  ("Cassidy Brooks","1-632-465-8248","bibendum.ullamcorper.duis@aol.edu","Costa Rica",9,"HRV57OVN5SD","$46.01"),
  ("Yardley Huffman","1-602-687-9373","elit@aol.couk","Poland",2,"BHX76AOE2HF","$2.61"),
  ("Samuel Lynch","(279) 250-9662","mauris.vestibulum.neque@aol.edu","India",1,"YTB36LIQ1NF","$21.68"),
  ("Michelle Powers","(615) 537-9482","est.ac@yahoo.edu","Norway",10,"MUV26MSQ6PL","$84.44"),
  ("Garrett Meadows","1-460-365-2167","rhoncus@google.couk","Peru",8,"VVK22QJI3KY","$86.02"),
  ("Norman Hanson","1-331-323-3815","dui.cum@yahoo.ca","South Korea",7,"KIH09LHI5US","$29.20");
INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("Catherine Huff","1-578-694-4219","duis@protonmail.ca","Pakistan",0,"TVT45VIN2RL","$8.15"),
  ("Ginger Maynard","(125) 410-1376","sodales.elit@google.edu","Germany",10,"PEI63NYL6MK","$62.33"),
  ("Harlan Livingston","1-967-516-8654","at.lacus.quisque@icloud.org","United Kingdom",5,"DPL07URN4HD","$58.92"),
  ("Chadwick Griffin","1-492-828-5877","sodales.at@google.com","Indonesia",3,"OTK82OBK3IL","$61.83"),
  ("Cameron Reyes","(917) 325-5619","ligula.elit@hotmail.couk","United Kingdom",9,"XJV48GXG4TH","$81.82"),
  ("Rowan Pratt","1-895-588-9318","nunc@protonmail.ca","Spain",3,"OYZ84IKL3FA","$33.10"),
  ("Conan Thompson","1-704-344-8175","ac.mattis@hotmail.edu","India",7,"XNN18WLB6KO","$89.22"),
  ("Josiah Delacruz","1-980-354-7806","diam.vel@hotmail.ca","India",6,"XOV04OGJ6OV","$14.87"),
  ("Shaeleigh Calderon","1-294-719-2553","sed.molestie@protonmail.ca","Pakistan",10,"KMI86CYF2XC","$50.55"),
  ("Juliet Stafford","1-666-822-2828","nunc.nulla@aol.edu","United States",10,"MQP48WBP5BY","$54.12");
INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("Isabella Bell","(936) 886-7422","sed.sem@hotmail.edu","United Kingdom",9,"OKM14BRU0ZB","$10.68"),
  ("Jarrod Fisher","1-705-542-7866","neque.vitae@google.com","Vietnam",0,"QAR55SWX5TO","$49.82"),
  ("Zachery Peters","1-208-221-6857","nec.malesuada@google.com","France",10,"YOJ16AYD7HX","$8.75"),
  ("Rogan Russell","(723) 533-2373","aenean.sed@google.org","Belgium",1,"MMT75POW5PM","$2.34"),
  ("Rama Paul","1-654-864-1113","vitae.odio@aol.net","Pakistan",9,"ORH44LHM3UI","$58.50"),
  ("Zachary Cunningham","(918) 335-6481","eleifend.egestas.sed@protonmail.edu","Indonesia",1,"UWF55VQM4OI","$51.96"),
  ("Amos Stark","1-723-644-5426","euismod.et.commodo@hotmail.couk","South Korea",9,"RCV53OTM6WO","$78.98"),
  ("Lane Oliver","1-666-673-4860","ornare.facilisis.eget@hotmail.com","China",8,"AMD57LED5JF","$87.77"),
  ("Tarik Marquez","(876) 835-4618","nec@aol.couk","France",7,"UMJ39XED3PB","$62.73"),
  ("Elmo Guy","1-604-986-1965","sed.id@yahoo.org","Costa Rica",4,"WVC22JUN4ER","$80.58");
INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("William Lancaster","(851) 236-6753","lectus.quis.massa@yahoo.org","India",5,"LIC80DDJ8BP","$61.63"),
  ("Anastasia Vance","(572) 466-9673","nunc.sollicitudin@hotmail.ca","China",1,"VVR34YBX1AR","$67.81"),
  ("Kristen Spence","1-417-257-4695","enim.consequat.purus@hotmail.edu","Belgium",6,"KVX88HWY9RE","$25.45"),
  ("Miranda Campos","1-832-540-8123","a.malesuada@outlook.org","New Zealand",8,"FEQ14JND2WK","$34.39"),
  ("Leila Contreras","(426) 847-1608","suspendisse.ac@aol.edu","Mexico",4,"CJE82LKO8SO","$76.11"),
  ("Oleg Mueller","1-494-677-9649","sem.eget@protonmail.net","Brazil",3,"DSQ32NER3XL","$0.61"),
  ("Kylan Wilcox","(942) 767-6205","quam.curabitur@yahoo.ca","Chile",3,"NDG24NCB2UF","$85.41"),
  ("Malcolm Keith","(686) 816-1735","etiam.imperdiet@aol.com","Vietnam",2,"BXH73CSL8JK","$65.43"),
  ("Troy Alvarado","1-553-767-7575","cursus@outlook.ca","Italy",6,"WZO58YUQ3DA","$81.57"),
  ("Tucker Bishop","(282) 227-6372","auctor@hotmail.couk","Ireland",5,"DGE22XWU1HP","$89.27");
INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("Colby Tillman","1-630-379-9021","sed@outlook.net","South Korea",1,"TWC55YWZ7MO","$21.56"),
  ("Octavia Nicholson","(868) 431-2777","ipsum.ac@yahoo.edu","Belgium",3,"HCH70KWB3HM","$54.44"),
  ("Brynne Beard","1-837-517-0763","velit.egestas.lacinia@icloud.couk","India",6,"EFW45OJD0AJ","$44.63"),
  ("Calista Whitehead","(851) 488-6471","phasellus@aol.org","Colombia",7,"QFH25UUC2XG","$0.92"),
  ("Kylie Fernandez","1-643-739-6636","varius.nam@hotmail.ca","Brazil",4,"WAP69IKO4DE","$21.39"),
  ("Coby Olson","(526) 224-2221","nunc.ac@aol.com","United Kingdom",8,"JRA46IAO2RE","$67.96"),
  ("Sonia Holden","(206) 253-5858","mauris.id.sapien@google.edu","Germany",1,"TUV43YWF1FO","$55.01"),
  ("Mark Avery","1-780-624-3167","dis.parturient@outlook.edu","India",7,"VSE59FOZ5CC","$58.90"),
  ("Eugenia Hudson","(514) 789-8568","ut.molestie@yahoo.ca","Spain",2,"MMI83DYG3JS","$23.73"),
  ("Cally Castaneda","1-925-582-3517","convallis.dolor@hotmail.couk","Norway",3,"XUZ07LHU9NK","$14.23");
INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("Chelsea Guerra","(798) 501-3173","ac.mattis.velit@outlook.couk","Vietnam",9,"AKK28YEK6KL","$40.72"),
  ("Justina Knight","1-477-470-5558","suspendisse@google.edu","United Kingdom",0,"DFQ71VCX3ZI","$62.23"),
  ("Warren Singleton","(834) 249-7937","mauris.eu.turpis@outlook.couk","India",1,"EBC41PWS7BE","$32.88"),
  ("Steven Barron","(371) 416-4578","eros.non.enim@icloud.net","Costa Rica",5,"JKL49YSG6SL","$57.89"),
  ("Vivien Cash","(597) 663-9117","in.sodales@google.com","Colombia",7,"OHZ49JLM4JS","$87.33"),
  ("Aphrodite Dominguez","1-476-257-4852","quisque.tincidunt@yahoo.com","Indonesia",9,"VIL16KVQ7ZN","$93.59"),
  ("Justine Kaufman","1-448-466-5269","ornare.elit@google.net","Germany",5,"OVU09EYC6MX","$3.38"),
  ("Eugenia Buck","1-405-711-9306","aliquam.tincidunt@hotmail.com","Ireland",5,"MGK15IYS3RJ","$64.95"),
  ("Kaye Arnold","1-506-823-1691","sed.orci.lobortis@icloud.net","Australia",8,"EEW12WTV8SW","$58.44"),
  ("Jade Baldwin","1-312-931-6269","dapibus@yahoo.org","Colombia",8,"MDS41OHV3GY","$97.19");
INSERT INTO `myTable` (`name`,`phone`,`email`,`country`,`numberrange`,`alphanumeric`,`currency`)
VALUES
  ("Dominique Lester","(657) 280-5869","consequat.enim@aol.couk","Belgium",10,"CSE21TKS9MH","$84.80"),
  ("Wyoming Vasquez","(957) 430-5768","neque.et.nunc@hotmail.ca","Vietnam",3,"HGH63MFX8QD","$41.94"),
  ("Jason Pace","(273) 537-5323","amet.dapibus@protonmail.com","United Kingdom",4,"NLX71USB9QF","$97.44"),
  ("Joseph Anderson","(961) 135-8448","mauris.non.dui@hotmail.net","India",3,"ZEB04YZV5NW","$48.74"),
  ("Kuame Freeman","(278) 171-4388","ornare@hotmail.org","Ireland",0,"BSY01WOY0NG","$84.04"),
  ("Ezekiel Ayers","(442) 258-6534","malesuada@aol.org","Vietnam",7,"GNB19RFW0YS","$56.96"),
  ("Meredith Lucas","1-863-381-4863","sed.et.libero@icloud.ca","Peru",3,"SJO66VLY4HC","$67.94"),
  ("Erich Alvarez","1-114-442-6817","porttitor.tellus.non@icloud.com","New Zealand",3,"NTY30QPK6YM","$33.02"),
  ("Emmanuel Koch","1-689-340-5718","phasellus.dolor.elit@aol.ca","India",2,"HJJ53WYD3ZY","$67.82"),
  ("Hayden Snow","(112) 553-2086","id.ante@icloud.net","Turkey",6,"OTZ04IZV2VD","$64.08");
